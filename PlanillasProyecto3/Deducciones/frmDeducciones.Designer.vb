﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeducciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CodDeduccionLabel As System.Windows.Forms.Label
        Dim MENSUALLabel As System.Windows.Forms.Label
        Dim CATORCENALLabel As System.Windows.Forms.Label
        Dim PERSONALLabel As System.Windows.Forms.Label
        Dim PATRONALLabel As System.Windows.Forms.Label
        Dim REBAJOEMBARGOLabel As System.Windows.Forms.Label
        Dim DEDUCCCOLEGIOLabel As System.Windows.Forms.Label
        Dim Fiesta_JefaturasLabel As System.Windows.Forms.Label
        Dim Fiesta_Fin_de_AñoLabel As System.Windows.Forms.Label
        Dim OTRAS_DEDUCCLabel As System.Windows.Forms.Label
        Dim FondoMutualLabel As System.Windows.Forms.Label
        Dim Préstamos_BcoPopularLabel As System.Windows.Forms.Label
        Dim SumaTotalLabel As System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.CodDeduccionTextBox = New System.Windows.Forms.TextBox()
        Me.DeduccionesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.MENSUALTextBox = New System.Windows.Forms.TextBox()
        Me.CATORCENALTextBox = New System.Windows.Forms.TextBox()
        Me.PERSONALTextBox = New System.Windows.Forms.TextBox()
        Me.PATRONALTextBox = New System.Windows.Forms.TextBox()
        Me.REBAJOEMBARGOTextBox = New System.Windows.Forms.TextBox()
        Me.DEDUCCCOLEGIOTextBox = New System.Windows.Forms.TextBox()
        Me.Fiesta_JefaturasTextBox = New System.Windows.Forms.TextBox()
        Me.Fiesta_Fin_de_AñoTextBox = New System.Windows.Forms.TextBox()
        Me.OTRAS_DEDUCCTextBox = New System.Windows.Forms.TextBox()
        Me.FondoMutualTextBox = New System.Windows.Forms.TextBox()
        Me.Préstamos_BcoPopularTextBox = New System.Windows.Forms.TextBox()
        Me.SumaTotalTextBox = New System.Windows.Forms.TextBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.RegresarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LimpiarCajasDeTextoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeduccionesTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.DeduccionesTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        CodDeduccionLabel = New System.Windows.Forms.Label()
        MENSUALLabel = New System.Windows.Forms.Label()
        CATORCENALLabel = New System.Windows.Forms.Label()
        PERSONALLabel = New System.Windows.Forms.Label()
        PATRONALLabel = New System.Windows.Forms.Label()
        REBAJOEMBARGOLabel = New System.Windows.Forms.Label()
        DEDUCCCOLEGIOLabel = New System.Windows.Forms.Label()
        Fiesta_JefaturasLabel = New System.Windows.Forms.Label()
        Fiesta_Fin_de_AñoLabel = New System.Windows.Forms.Label()
        OTRAS_DEDUCCLabel = New System.Windows.Forms.Label()
        FondoMutualLabel = New System.Windows.Forms.Label()
        Préstamos_BcoPopularLabel = New System.Windows.Forms.Label()
        SumaTotalLabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DeduccionesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CodDeduccionLabel
        '
        CodDeduccionLabel.AutoSize = True
        CodDeduccionLabel.Location = New System.Drawing.Point(108, 28)
        CodDeduccionLabel.Name = "CodDeduccionLabel"
        CodDeduccionLabel.Size = New System.Drawing.Size(83, 13)
        CodDeduccionLabel.TabIndex = 0
        CodDeduccionLabel.Text = "cod Deduccion:"
        '
        'MENSUALLabel
        '
        MENSUALLabel.AutoSize = True
        MENSUALLabel.Location = New System.Drawing.Point(108, 54)
        MENSUALLabel.Name = "MENSUALLabel"
        MENSUALLabel.Size = New System.Drawing.Size(62, 13)
        MENSUALLabel.TabIndex = 2
        MENSUALLabel.Text = "MENSUAL:"
        '
        'CATORCENALLabel
        '
        CATORCENALLabel.AutoSize = True
        CATORCENALLabel.Location = New System.Drawing.Point(108, 80)
        CATORCENALLabel.Name = "CATORCENALLabel"
        CATORCENALLabel.Size = New System.Drawing.Size(82, 13)
        CATORCENALLabel.TabIndex = 4
        CATORCENALLabel.Text = "CATORCENAL:"
        '
        'PERSONALLabel
        '
        PERSONALLabel.AutoSize = True
        PERSONALLabel.Location = New System.Drawing.Point(108, 106)
        PERSONALLabel.Name = "PERSONALLabel"
        PERSONALLabel.Size = New System.Drawing.Size(68, 13)
        PERSONALLabel.TabIndex = 6
        PERSONALLabel.Text = "PERSONAL:"
        '
        'PATRONALLabel
        '
        PATRONALLabel.AutoSize = True
        PATRONALLabel.Location = New System.Drawing.Point(108, 132)
        PATRONALLabel.Name = "PATRONALLabel"
        PATRONALLabel.Size = New System.Drawing.Size(68, 13)
        PATRONALLabel.TabIndex = 8
        PATRONALLabel.Text = "PATRONAL:"
        '
        'REBAJOEMBARGOLabel
        '
        REBAJOEMBARGOLabel.AutoSize = True
        REBAJOEMBARGOLabel.Location = New System.Drawing.Point(108, 158)
        REBAJOEMBARGOLabel.Name = "REBAJOEMBARGOLabel"
        REBAJOEMBARGOLabel.Size = New System.Drawing.Size(106, 13)
        REBAJOEMBARGOLabel.TabIndex = 10
        REBAJOEMBARGOLabel.Text = "REBAJOEMBARGO:"
        '
        'DEDUCCCOLEGIOLabel
        '
        DEDUCCCOLEGIOLabel.AutoSize = True
        DEDUCCCOLEGIOLabel.Location = New System.Drawing.Point(108, 184)
        DEDUCCCOLEGIOLabel.Name = "DEDUCCCOLEGIOLabel"
        DEDUCCCOLEGIOLabel.Size = New System.Drawing.Size(102, 13)
        DEDUCCCOLEGIOLabel.TabIndex = 12
        DEDUCCCOLEGIOLabel.Text = "DEDUCCCOLEGIO:"
        '
        'Fiesta_JefaturasLabel
        '
        Fiesta_JefaturasLabel.AutoSize = True
        Fiesta_JefaturasLabel.Location = New System.Drawing.Point(108, 210)
        Fiesta_JefaturasLabel.Name = "Fiesta_JefaturasLabel"
        Fiesta_JefaturasLabel.Size = New System.Drawing.Size(84, 13)
        Fiesta_JefaturasLabel.TabIndex = 14
        Fiesta_JefaturasLabel.Text = "Fiesta Jefaturas:"
        '
        'Fiesta_Fin_de_AñoLabel
        '
        Fiesta_Fin_de_AñoLabel.AutoSize = True
        Fiesta_Fin_de_AñoLabel.Location = New System.Drawing.Point(108, 236)
        Fiesta_Fin_de_AñoLabel.Name = "Fiesta_Fin_de_AñoLabel"
        Fiesta_Fin_de_AñoLabel.Size = New System.Drawing.Size(92, 13)
        Fiesta_Fin_de_AñoLabel.TabIndex = 16
        Fiesta_Fin_de_AñoLabel.Text = "Fiesta Fin de Año:"
        '
        'OTRAS_DEDUCCLabel
        '
        OTRAS_DEDUCCLabel.AutoSize = True
        OTRAS_DEDUCCLabel.Location = New System.Drawing.Point(108, 262)
        OTRAS_DEDUCCLabel.Name = "OTRAS_DEDUCCLabel"
        OTRAS_DEDUCCLabel.Size = New System.Drawing.Size(95, 13)
        OTRAS_DEDUCCLabel.TabIndex = 18
        OTRAS_DEDUCCLabel.Text = "OTRAS DEDUCC:"
        '
        'FondoMutualLabel
        '
        FondoMutualLabel.AutoSize = True
        FondoMutualLabel.Location = New System.Drawing.Point(108, 288)
        FondoMutualLabel.Name = "FondoMutualLabel"
        FondoMutualLabel.Size = New System.Drawing.Size(75, 13)
        FondoMutualLabel.TabIndex = 20
        FondoMutualLabel.Text = "Fondo Mutual:"
        '
        'Préstamos_BcoPopularLabel
        '
        Préstamos_BcoPopularLabel.AutoSize = True
        Préstamos_BcoPopularLabel.Location = New System.Drawing.Point(108, 314)
        Préstamos_BcoPopularLabel.Name = "Préstamos_BcoPopularLabel"
        Préstamos_BcoPopularLabel.Size = New System.Drawing.Size(120, 13)
        Préstamos_BcoPopularLabel.TabIndex = 22
        Préstamos_BcoPopularLabel.Text = "Préstamos Bco Popular:"
        '
        'SumaTotalLabel
        '
        SumaTotalLabel.AutoSize = True
        SumaTotalLabel.Location = New System.Drawing.Point(108, 340)
        SumaTotalLabel.Name = "SumaTotalLabel"
        SumaTotalLabel.Size = New System.Drawing.Size(64, 13)
        SumaTotalLabel.TabIndex = 24
        SumaTotalLabel.Text = "Suma Total:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnAgregar)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.btnActualizar)
        Me.GroupBox1.Controls.Add(Me.btnBorrar)
        Me.GroupBox1.Controls.Add(CodDeduccionLabel)
        Me.GroupBox1.Controls.Add(Me.CodDeduccionTextBox)
        Me.GroupBox1.Controls.Add(MENSUALLabel)
        Me.GroupBox1.Controls.Add(Me.MENSUALTextBox)
        Me.GroupBox1.Controls.Add(CATORCENALLabel)
        Me.GroupBox1.Controls.Add(Me.CATORCENALTextBox)
        Me.GroupBox1.Controls.Add(PERSONALLabel)
        Me.GroupBox1.Controls.Add(Me.PERSONALTextBox)
        Me.GroupBox1.Controls.Add(PATRONALLabel)
        Me.GroupBox1.Controls.Add(Me.PATRONALTextBox)
        Me.GroupBox1.Controls.Add(REBAJOEMBARGOLabel)
        Me.GroupBox1.Controls.Add(Me.REBAJOEMBARGOTextBox)
        Me.GroupBox1.Controls.Add(DEDUCCCOLEGIOLabel)
        Me.GroupBox1.Controls.Add(Me.DEDUCCCOLEGIOTextBox)
        Me.GroupBox1.Controls.Add(Fiesta_JefaturasLabel)
        Me.GroupBox1.Controls.Add(Me.Fiesta_JefaturasTextBox)
        Me.GroupBox1.Controls.Add(Fiesta_Fin_de_AñoLabel)
        Me.GroupBox1.Controls.Add(Me.Fiesta_Fin_de_AñoTextBox)
        Me.GroupBox1.Controls.Add(OTRAS_DEDUCCLabel)
        Me.GroupBox1.Controls.Add(Me.OTRAS_DEDUCCTextBox)
        Me.GroupBox1.Controls.Add(FondoMutualLabel)
        Me.GroupBox1.Controls.Add(Me.FondoMutualTextBox)
        Me.GroupBox1.Controls.Add(Préstamos_BcoPopularLabel)
        Me.GroupBox1.Controls.Add(Me.Préstamos_BcoPopularTextBox)
        Me.GroupBox1.Controls.Add(SumaTotalLabel)
        Me.GroupBox1.Controls.Add(Me.SumaTotalTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 40)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(504, 403)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ingrese los datos"
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(135, 365)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregar.TabIndex = 13
        Me.btnAgregar.Text = "&Ingregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(216, 365)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 14
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'btnActualizar
        '
        Me.btnActualizar.Location = New System.Drawing.Point(296, 365)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(75, 23)
        Me.btnActualizar.TabIndex = 15
        Me.btnActualizar.Text = "&Actualizar"
        Me.btnActualizar.UseVisualStyleBackColor = True
        '
        'btnBorrar
        '
        Me.btnBorrar.Location = New System.Drawing.Point(377, 365)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(75, 23)
        Me.btnBorrar.TabIndex = 16
        Me.btnBorrar.Text = "&Borrar"
        Me.btnBorrar.UseVisualStyleBackColor = True
        '
        'CodDeduccionTextBox
        '
        Me.CodDeduccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "codDeduccion", True))
        Me.CodDeduccionTextBox.Location = New System.Drawing.Point(234, 25)
        Me.CodDeduccionTextBox.Name = "CodDeduccionTextBox"
        Me.CodDeduccionTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CodDeduccionTextBox.TabIndex = 0
        '
        'DeduccionesBindingSource
        '
        Me.DeduccionesBindingSource.DataMember = "Deducciones"
        Me.DeduccionesBindingSource.DataSource = Me.Planilla2DataSet
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MENSUALTextBox
        '
        Me.MENSUALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "MENSUAL", True))
        Me.MENSUALTextBox.Location = New System.Drawing.Point(234, 51)
        Me.MENSUALTextBox.Name = "MENSUALTextBox"
        Me.MENSUALTextBox.Size = New System.Drawing.Size(100, 20)
        Me.MENSUALTextBox.TabIndex = 1
        '
        'CATORCENALTextBox
        '
        Me.CATORCENALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "CATORCENAL", True))
        Me.CATORCENALTextBox.Location = New System.Drawing.Point(234, 77)
        Me.CATORCENALTextBox.Name = "CATORCENALTextBox"
        Me.CATORCENALTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CATORCENALTextBox.TabIndex = 2
        '
        'PERSONALTextBox
        '
        Me.PERSONALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "PERSONAL", True))
        Me.PERSONALTextBox.Location = New System.Drawing.Point(234, 103)
        Me.PERSONALTextBox.Name = "PERSONALTextBox"
        Me.PERSONALTextBox.Size = New System.Drawing.Size(100, 20)
        Me.PERSONALTextBox.TabIndex = 3
        '
        'PATRONALTextBox
        '
        Me.PATRONALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "PATRONAL", True))
        Me.PATRONALTextBox.Location = New System.Drawing.Point(234, 129)
        Me.PATRONALTextBox.Name = "PATRONALTextBox"
        Me.PATRONALTextBox.Size = New System.Drawing.Size(100, 20)
        Me.PATRONALTextBox.TabIndex = 4
        '
        'REBAJOEMBARGOTextBox
        '
        Me.REBAJOEMBARGOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "REBAJOEMBARGO", True))
        Me.REBAJOEMBARGOTextBox.Location = New System.Drawing.Point(234, 155)
        Me.REBAJOEMBARGOTextBox.Name = "REBAJOEMBARGOTextBox"
        Me.REBAJOEMBARGOTextBox.Size = New System.Drawing.Size(100, 20)
        Me.REBAJOEMBARGOTextBox.TabIndex = 5
        '
        'DEDUCCCOLEGIOTextBox
        '
        Me.DEDUCCCOLEGIOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "DEDUCCCOLEGIO", True))
        Me.DEDUCCCOLEGIOTextBox.Location = New System.Drawing.Point(234, 181)
        Me.DEDUCCCOLEGIOTextBox.Name = "DEDUCCCOLEGIOTextBox"
        Me.DEDUCCCOLEGIOTextBox.Size = New System.Drawing.Size(100, 20)
        Me.DEDUCCCOLEGIOTextBox.TabIndex = 6
        '
        'Fiesta_JefaturasTextBox
        '
        Me.Fiesta_JefaturasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "Fiesta Jefaturas", True))
        Me.Fiesta_JefaturasTextBox.Location = New System.Drawing.Point(234, 207)
        Me.Fiesta_JefaturasTextBox.Name = "Fiesta_JefaturasTextBox"
        Me.Fiesta_JefaturasTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Fiesta_JefaturasTextBox.TabIndex = 7
        '
        'Fiesta_Fin_de_AñoTextBox
        '
        Me.Fiesta_Fin_de_AñoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "Fiesta Fin de Año", True))
        Me.Fiesta_Fin_de_AñoTextBox.Location = New System.Drawing.Point(234, 233)
        Me.Fiesta_Fin_de_AñoTextBox.Name = "Fiesta_Fin_de_AñoTextBox"
        Me.Fiesta_Fin_de_AñoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Fiesta_Fin_de_AñoTextBox.TabIndex = 8
        '
        'OTRAS_DEDUCCTextBox
        '
        Me.OTRAS_DEDUCCTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "OTRAS DEDUCC", True))
        Me.OTRAS_DEDUCCTextBox.Location = New System.Drawing.Point(234, 259)
        Me.OTRAS_DEDUCCTextBox.Name = "OTRAS_DEDUCCTextBox"
        Me.OTRAS_DEDUCCTextBox.Size = New System.Drawing.Size(100, 20)
        Me.OTRAS_DEDUCCTextBox.TabIndex = 9
        '
        'FondoMutualTextBox
        '
        Me.FondoMutualTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "FondoMutual", True))
        Me.FondoMutualTextBox.Location = New System.Drawing.Point(234, 285)
        Me.FondoMutualTextBox.Name = "FondoMutualTextBox"
        Me.FondoMutualTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FondoMutualTextBox.TabIndex = 10
        '
        'Préstamos_BcoPopularTextBox
        '
        Me.Préstamos_BcoPopularTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "Préstamos BcoPopular", True))
        Me.Préstamos_BcoPopularTextBox.Location = New System.Drawing.Point(234, 311)
        Me.Préstamos_BcoPopularTextBox.Name = "Préstamos_BcoPopularTextBox"
        Me.Préstamos_BcoPopularTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Préstamos_BcoPopularTextBox.TabIndex = 11
        '
        'SumaTotalTextBox
        '
        Me.SumaTotalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DeduccionesBindingSource, "SumaTotal", True))
        Me.SumaTotalTextBox.Location = New System.Drawing.Point(234, 337)
        Me.SumaTotalTextBox.Name = "SumaTotalTextBox"
        Me.SumaTotalTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SumaTotalTextBox.TabIndex = 12
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 467)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(556, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegresarToolStripMenuItem, Me.ConsultarToolStripMenuItem, Me.LimpiarCajasDeTextoToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(556, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'RegresarToolStripMenuItem
        '
        Me.RegresarToolStripMenuItem.Name = "RegresarToolStripMenuItem"
        Me.RegresarToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.RegresarToolStripMenuItem.Text = "&Regresar"
        '
        'ConsultarToolStripMenuItem
        '
        Me.ConsultarToolStripMenuItem.Name = "ConsultarToolStripMenuItem"
        Me.ConsultarToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.ConsultarToolStripMenuItem.Text = "&Consultar"
        '
        'LimpiarCajasDeTextoToolStripMenuItem
        '
        Me.LimpiarCajasDeTextoToolStripMenuItem.Name = "LimpiarCajasDeTextoToolStripMenuItem"
        Me.LimpiarCajasDeTextoToolStripMenuItem.Size = New System.Drawing.Size(133, 20)
        Me.LimpiarCajasDeTextoToolStripMenuItem.Text = "&Limpiar cajas de texto"
        '
        'DeduccionesTableAdapter
        '
        Me.DeduccionesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Nothing
        Me.TableAdapterManager.DeduccionesTableAdapter = Me.DeduccionesTableAdapter
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'frmDeducciones
        '
        Me.AcceptButton = Me.btnAgregar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(556, 489)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "frmDeducciones"
        Me.Text = "frmDeducciones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DeduccionesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents RegresarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents DeduccionesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DeduccionesTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.DeduccionesTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents CodDeduccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MENSUALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CATORCENALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PERSONALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PATRONALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents REBAJOEMBARGOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DEDUCCCOLEGIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fiesta_JefaturasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fiesta_Fin_de_AñoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OTRAS_DEDUCCTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FondoMutualTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Préstamos_BcoPopularTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SumaTotalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents btnActualizar As System.Windows.Forms.Button
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents LimpiarCajasDeTextoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
