﻿Public Class frmDatosEmpleadoConsulta
    Public datocedula As Integer

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmDatosEmpleadoConsulta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            Me.DatosPersonalesTableAdapter.FillBy1BuscarPersona(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(txtDatoConsulta.Text))
        Catch ex As Exception
            MsgBox("Verifique los parametros de busqueda: " + ex.Message)
        End Try
    End Sub

    Private Sub btnMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMostrar.Click

    End Sub

    Private Sub DatosPersonalesDataGridView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DatosPersonalesDataGridView.DoubleClick
        'Dim cedula As Integer = Integer.Parse(DatosPersonalesDataGridView.Item(0, DatosPersonalesDataGridView.CurrentRow.Index).Value)
        datocedula = Integer.Parse(DatosPersonalesDataGridView.Item(0, DatosPersonalesDataGridView.CurrentRow.Index).Value)
        MsgBox(datocedula)
        My.Forms.formdatosEmpleadosActualizar.CelularTextBox.Text = datocedula
        My.Forms.formdatosEmpleadosActualizar.Show()      
    End Sub

   
   
End Class