﻿Public Class formdatosEmpleadosActualizar
    Dim cedula As Integer

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub formdatosEmpleadosActualizar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
        txtDatoConsulta.Text = frmDatosEmpleadoConsulta.datocedula.ToString

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            Me.DatosPersonalesTableAdapter.FillBy1BuscarPersona(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(txtDatoConsulta.Text))
        Catch ex As Exception
            MsgBox("Verifique los parametros de busqueda: " + ex.Message)
        End Try
    End Sub

    
    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        frmDatosEmpleadoConsulta.Show()
        Me.Hide()
    End Sub
End Class