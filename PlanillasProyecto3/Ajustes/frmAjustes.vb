﻿Public Class frmAjustes

    Private data As Integer
    Private diario As Double
    Private status As Boolean
    Private diferencia As Double


    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmAjustes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
       
        'TODO: This line of code loads data into the 'Planilla2DataSet1.Puestos' table. You can move, or remove it, as needed.
        Me.PuestosTableAdapter1.Fill(Me.Planilla2DataSet1.Puestos)
        'TODO: This line of code loads data into the 'Planilla2DataSetPuestos.Puestos' table. You can move, or remove it, as needed.
        'TODO: This line of code loads data into the 'Planilla2DataSet.Ajustes' table. You can move, or remove it, as needed.
        Me.AjustesTableAdapter.Fill(Me.Planilla2DataSet.Ajustes)
        'TODO: This line of code loads data into the 'Planilla2DataSet.Puestos' table. You can move, or remove it, as needed.
        Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)
        'TODO: This line of code loads data into the 'Planilla2DataSet.DatosPersonales' table. You can move, or remove it, as needed.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)
        limpiaCajasTextoS()
    End Sub

    Private Sub limpiaCajasTextoS()
        CodPUESTO_NUEVOTextBox.Text = 0
        CodCedulaAjusteTextBox.Text = 0
        NoCEDULATextBox.Text = 0

        NOMBRE_COMPLETOTextBox.Clear()
        E_MAILTextBox.Clear()
        CODPUESTOTextBox.Clear()
        GRADO_ACADEMICOTextBox.Clear()
        GRADO_ACADEMICOTextBox1.Clear()
        GRADO_ACADREQUERIDOTextBox.Clear()
        TelefonoTextBox.Clear()
        CelularTextBox.Clear()
        DireccionTextBox.Clear()
        CODPUESTOTextBox.Text = 0
        DESCRIPCIONTextBox.Clear()
        DEPARTAMENTOTextBox.Clear()
        SALARIO_UNICOTextBox.Clear()
        EMPLEADO_CONFIANZACheckBox.Checked = False
        GRADO_ACADEMICOTextBox.Clear()

         DESCRIPCIONPUESTONUEVOTextBox.Clear()
        SALARIO_NUEVOTextBox.Clear()
        GRADO_ACADEMICOTextBox.Clear()
        _CUMPLE_REQUISITO_CheckBox.Checked = False

        TDIASAJUSTARTextBox.Clear()
        DiferenciaTextBox.Clear()
        DiarioTextBox.Clear()
        TOTALAJUSTARTextBox.Clear()


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarEmpleado.Click

        Try

            data = CODPUESTOTextBox.Text


            NoCEDULATextBox.Text = InputBox("Ingrese la cedula")



            If Me.DatosPersonalesTableAdapter.FillBy(Me.Planilla2DataSet.DatosPersonales, Integer.Parse(NoCEDULATextBox.Text)) = False Then
                MsgBox("No se encontro el emppleado")
                NoCEDULATextBox.Text = 0
                CODPUESTOTextBox.Text = 0
                CodCedulaAjusteTextBox.Text = Integer.Parse(NoCEDULATextBox.Text)
            Else
                CodCedulaAjusteTextBox.Text = Integer.Parse(NoCEDULATextBox.Text)

            End If


        Catch ex As Exception
            MsgBox("Verifique los datos ingresados")
        End Try

        Try

            Me.PuestosTableAdapter.FillBy(Me.Planilla2DataSet.Puestos, Integer.Parse(CODPUESTOTextBox.Text))
        Catch ex As Exception
            NoCEDULATextBox.Text = 0
        End Try


    End Sub

    Private Sub CODPUESTOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.PuestosTableAdapter.FillBy(Me.Planilla2DataSet.Puestos, Integer.Parse(CODPUESTOTextBox.Text))

    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Try
            Me.PuestosTableAdapter1.FillByPuesto(Me.Planilla2DataSet1.Puestos, Integer.Parse(InputBox("Ingrese el codigo Puesto")))
            Me.data = Integer.Parse(CODPUESTOTextBox.Text)

        Catch ex As Exception
            MsgBox("Verifique los datos")
        End Try
    End Sub

    Private Sub CODPUESTOTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CODPUESTOTextBox.TextChanged
        Me.PuestosTableAdapter.FillBy(Me.Planilla2DataSet.Puestos, data)

    End Sub


    Sub ajustarpPuesto()

        Try




            If _CUMPLE_REQUISITO_CheckBox.Checked = True Then

                If Integer.Parse(TDIASAJUSTARTextBox.Text) > 7 Then

                    If Integer.Parse(TDIASAJUSTARTextBox.Text) <= 15 Then


                        If diferencia >= 0 Then
                            diferencia = Double.Parse(SALARIO_NUEVOTextBox.Text) - Double.Parse(SALARIO_UNICOTextBox.Text)
                            DiferenciaTextBox.Text = diferencia
                            diario = Format(Double.Parse(diferencia) / 15, "##,##")
                            DiarioTextBox.Text = diario
                            TOTALAJUSTARTextBox.Text = diario * Double.Parse(TDIASAJUSTARTextBox.Text)
                        Else
                            diferencia = Double.Parse(SALARIO_UNICOTextBox.Text) - Double.Parse(SALARIO_NUEVOTextBox.Text)
                            DiferenciaTextBox.Text = diferencia
                            diario = Format(Double.Parse(diferencia) / 15, "##,##")
                            DiarioTextBox.Text = diario
                            TOTALAJUSTARTextBox.Text = diario * Double.Parse(TDIASAJUSTARTextBox.Text)

                        End If
                    Else

                    End If

                End If

            ElseIf _CUMPLE_REQUISITO_CheckBox.Checked = False Then
                status = False

                If Integer.Parse(TDIASAJUSTARTextBox.Text) > 7 Then

                    If Integer.Parse(TDIASAJUSTARTextBox.Text) <= 15 Then



                        If True Then

                            diferencia = (Double.Parse(SALARIO_NUEVOTextBox.Text) - Double.Parse(SALARIO_UNICOTextBox.Text)) * (50) / 100
                            DiferenciaTextBox.Text = diferencia
                            diario = Format(Integer.Parse(diferencia) / 15, "##,#")
                            DiarioTextBox.Text = diario

                            TOTALAJUSTARTextBox.Text = diario * Double.Parse(TDIASAJUSTARTextBox.Text)
                        End If

                       
                    Else

                    End If
                Else
                    DiferenciaTextBox.Text = "No hay calculo"

                End If
            End If
        Catch ex As Exception

        End Try
       
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalcular.Click
        ajustarpPuesto()
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Try

            Me.AjustesTableAdapter.InsertQuery(Integer.Parse(CodCedulaAjusteTextBox.Text), Integer.Parse(CodPUESTO_NUEVOTextBox.Text), DESCRIPCIONPUESTONUEVOTextBox.Text, Integer.Parse(SALARIO_NUEVOTextBox.Text), GRADO_ACADEMICOTextBox.Text, status, CType(DiferenciaTextBox.Text, Integer), diario, Integer.Parse(TDIASAJUSTARTextBox.Text), Integer.Parse(TOTALAJUSTARTextBox.Text))
            MsgBox("Datos almacenados exitosamente")
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub RegresarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegresarToolStripMenuItem.Click
        frmPrincipal.Show()
        Me.Close()

    End Sub

    Private Sub ConsularToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsularToolStripMenuItem.Click
        frmConsulataAjustes.Show()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try


            Me.AjustesTableAdapter.FillBy(Me.Planilla2DataSet.Ajustes, Integer.Parse(CodCedulaAjusteTextBox.Text))
            MsgBox("Empleado encontrado")


        Catch ex As Exception
            MsgBox("Error" + ex.Message)

        End Try

    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Try
            Me.Validate()
            Me.DatosPersonalesBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)
            MsgBox("Los datos modificados, se han actualizado")
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try

    End Sub

    Private Sub btnBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBorrar.Click
        Try
            Dim dato As Integer

            dato = InputBox("Ingrese el cod Empleado que desea borrar")

            If MsgBox("Desea borrar el registro", vbOK) = vbYes Then
                Me.AjustesTableAdapter.DeleteQuery(Integer.Parse(dato))
            End If
        Catch ex As Exception
            MsgBox("Error" + ex.Message)
        End Try
    End Sub
End Class