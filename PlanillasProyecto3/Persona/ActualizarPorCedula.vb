﻿Public Class ActualizarPorCedula

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        ActualizarPorCedulaPersona()
    End Sub

    Private Sub DatosPersonalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.DatosPersonalesBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub ActualizarPorCedula_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Planilla2DataSet.DatosPersonales' Puede moverla o quitarla según sea necesario.
        Me.DatosPersonalesTableAdapter.Fill(Me.Planilla2DataSet.DatosPersonales)

    End Sub

    Private Sub ActualizarPorCedula_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed

        Principal.Show()
    End Sub

    Private Sub NoCEDULATextBox_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles TelefonoTextBox.Validating, NOMBRETextBox.Validating, NoCUENTATextBox.Validating, NoCEDULATextBox.Validating, N__HIJOSTextBox.Validating, GRADO_ACADEMICOTextBox.Validating, E_MAILTextBox.Validating, DireccionTextBox.Validating, CODPUESTOTextBox.Validating, CelularTextBox.Validating, BANCO_A_DEPOSITARTextBox.Validating, _2__APELLIDOTextBox.Validating, _1__APELLIDOTextBox.Validating
        Dim cajaTexto As TextBox = CType(sender, TextBox)


        If cajaTexto.Text = "" Then
            e.Cancel = True
            proveedorError.SetError(cajaTexto, "Campo Requerido")
            cajaTexto.Focus()
        End If

    End Sub

    Private Sub txtNom_ValidatinigNumeros(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles CODPUESTOTextBox.Validating, N__HIJOSTextBox.Validating, NoCEDULATextBox.Validating

        Dim cajaTexto As TextBox = CType(sender, TextBox)
        Dim dato As Integer
        Try
            dato = Integer.Parse(cajaTexto.Text)
        Catch ex As Exception
            e.Cancel = True
            proveedorError.SetError(cajaTexto, "Campo Númerico")
            cajaTexto.Focus()
        End Try


    End Sub

    Private Sub ActualizarPorCedula_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Validated
        proveedorError.Clear()
    End Sub
End Class